#!/usr/bin/env python

# __main__.py
# Evigilo v. 0.1.0
# (C) 202306 NoxNivi
# Evigilo is a simple alert program

# This alert is meant to be launched from the terminal
# or as a commandline process from any software
# Takes two parameters:
#     type:   alert type
#             info/warning/danger
#     text:   the alert text limited by double/single quotes
#
# Depending on the type a different color is set on the
# window, and a different icon is shown

# base imports
import sys
import importlib.resources

# local imports
import resources

# from imports
# from PySide6 import QtGui
from PySide6.QtGui import QGuiApplication,QImage
from PySide6.QtQml import QQmlApplicationEngine, QmlElement
from PySide6.QtCore import Signal, QObject, Slot, Property, QFile
from pathlib import Path


# evigilo name and version
_NAME = "evigilo"
_VERSION = "0.1.0"

# red color codes for terminal error messages
COLOR_RED = '\33[31m'
COLOR_END = '\33[0m'
ERROR = COLOR_RED + '[Error]' + COLOR_END


def check_params(parameters):
    """
      Checks the provided parameters to validate them
    """
    # base information
    alert_type = ["info", "warning", "danger", "help"]

    if len(params) == 0:
        show_help()
        sys.exit(0)
    else:
        if parameters[0] not in alert_type:
            show_error(1)
            return True
        else:
            return False


def show_error(errcode):
    """
      Shows the error code in the terminal according to the code
      provided by checkparams
    """
    error_codes = [
      ERROR + ": Too many arguments",
      ERROR + ": Type not recognized"
    ]
    if errcode == 0:
        print(error_codes[0])
    elif errcode == 1:
        print(error_codes[1])
    else:
        pass


def show_help():
    """
      Shows the help on how to use the app
    """
    print("Basic <evigilo> usage information")
    print(_NAME + " v." + _VERSION)
    print("usage: evigilo <option> \"message\" (message should be inside quotes)")
    print("options:")
    print("     info: creates an Info Alert with the message \"message\"")
    print("     warning: creates a Warning Alert with the message \"message\"")
    print("     danger: creates a Danger Alert with the message \"message\"")
    print("     help: shows this help")


def manage_qml_properties(params):
    """
      Gets the parameters and prepares the properties to be
      passed to the QML gui.
      Gets:
           parameters
      Returns:
           icon path
           message type
           message color
           message text
    """
    # the list or properties we are going to pass
    property_values = []

    alert_type = ["info", "warning", "danger", "help"]
    alert_color = ["blue", "yellow", "red"]

    # ../assets/alert-info.svg

    alert_icon = [
        "qrc:/assets/assets/alert-info.svg",
        "qrc:/assets/assets/alert-warning.svg",
        "qrc:/assets/assets/alert-danger.svg"
    ]

    if params[0] == alert_type[3]:
        show_help()
        sys.exit(0)
    else:
        if params[0] == alert_type[0]:
            property_values = [ "Info",
                                alert_color[0],
                                alert_icon[0],
                                params[1]
                                ]
        if params[0] == alert_type[1]:
            property_values = [ "Warning",
                                alert_color[1],
                                alert_icon[1],
                                params[1]
                                ]
        if params[0] == alert_type[2]:
            property_values = [ "Danger",
                                alert_color[2],
                                alert_icon[2],
                                params[1]
                              ]

        # print(property_values)
        return property_values


if __name__ == "__main__":
    # print('Evigilo')
    params = sys.argv[1:]
    # print("params = ",  params)
    is_error = check_params(params)
    if is_error:
        sys.exit()
    else:
        app = QGuiApplication(sys.argv)
        engine = QQmlApplicationEngine()

        engine.quit.connect(app.quit)

        qml_gui_file = importlib.resources.files("gui").joinpath("main.qml")
        qml_file = importlib.resources.as_file(qml_gui_file)

        with qml_file as qfile:
            engine.load(qfile)

        properties = manage_qml_properties(params)
        btype = properties[0]
        bcolor = properties[1]
        bicon = properties[2]
        bmessage = properties[3]

        engine.rootObjects()[0].setProperty('btype', btype)
        engine.rootObjects()[0].setProperty('bcolor', bcolor)
        engine.rootObjects()[0].setProperty('bicon', bicon)
        engine.rootObjects()[0].setProperty('bmessage', bmessage)

        exit_code = app.exec()
        del engine
        sys.exit(exit_code)
    
