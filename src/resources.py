# Resource object code (Python 3)
# Created by: object code
# Created by: The Resource Compiler for Qt version 6.5.1
# WARNING! All changes made in this file will be lost!

from PySide6 import QtCore

qt_resource_data = b"\
\x00\x00\x06\xe1\
/\
/ main.qml\x0a//\x0a//\
 evigilo v. 0.1.\
0\x0a// (C) 202306 \
NoxNivi\x0a//\x0a// GU\
I file for the s\
imple Alert app\x0a\
\x0a// imports\x0aimpo\
rt QtQuick\x0aimpor\
t QtQuick.Window\
\x0aimport QtQuick.\
Controls\x0aimport \
QtQuick.Layouts\x0a\
\x0a// local import\
s\x0a\x0aWindow {\x0a  id\
: root\x0a  width: \
480\x0a  height: 18\
0\x0a\x0a  property st\
ring bcolor: \x22\x22\x0a\
  property strin\
g btype: \x22\x22\x0a  pr\
operty string bm\
essage: \x22\x22\x0a  pro\
perty string bic\
on: \x22\x22\x0a\x0a  Compon\
ent.onCompleted:\
 visible = true\x0a\
\x0a  Pane {\x0a    id\
: rootContent\x0a  \
  anchors.fill: \
parent\x0a    paddi\
ng: 0\x0a\x0a    Colum\
nLayout {\x0a      \
id: colContent\x0a \
     anchors.fil\
l: parent\x0a      \
spacing: 10\x0a\x0a   \
   RowLayout {\x0a \
       id: alert\
Content\x0a        \
Layout.fillHeigh\
t: true\x0a        \
spacing: 20\x0a\x0a   \
     Image {\x0a   \
       id: imIco\
n\x0a          sour\
ce: bicon\x0a      \
  }\x0a\x0a        Col\
umnLayout {\x0a    \
      id: rightS\
ide\x0a          La\
yout.fillHeight:\
 true\x0a          \
Layout.fillWidth\
: true\x0a         \
 Layout.margins:\
 20\x0a          La\
yout.leftMargin:\
 0\x0a          Lay\
out.bottomMargin\
: 0\x0a          sp\
acing: 10\x0a\x0a     \
     Label {\x0a   \
         id: ale\
rtLabel\x0a        \
    Layout.fillW\
idth: true\x0a     \
       text: bty\
pe\x0a            f\
ont.pixelSize: a\
lertMessage.font\
.pixelSize * 1.5\
\x0a            fon\
t.bold: true\x0a   \
       }\x0a\x0a      \
    Text {\x0a     \
       id: alert\
Message\x0a        \
    Layout.fillH\
eight: true\x0a    \
        Layout.f\
illWidth: true\x0a \
           text:\
 bmessage\x0a      \
      wrapMode: \
Text.WordWrap\x0a  \
        }\x0a\x0a     \
     Button {\x0a  \
          id: qu\
itButton\x0a       \
     text: \x22OK\x22\x0a\
            Layo\
ut.alignment: Qt\
.AlignRight\x0a\x0a   \
         onClick\
ed: {\x0a          \
    // console.l\
og(\x22exit clicked\
\x22)\x0a             \
 Qt.quit()\x0a\x0a    \
        }\x0a      \
    }\x0a        }\x0a\
\x0a      }\x0a\x0a      \
Rectangle {\x0a    \
    id: colorCod\
e\x0a        height\
: 12\x0a        Lay\
out.fillWidth: t\
rue\x0a\x0a        col\
or: bcolor\x0a\x0a    \
  }\x0a    }\x0a  }\x0a}\x0a\
\
\x00\x00\x04\x86\
<\
svg version=\x221.1\
\x22 viewBox=\x220 0 1\
28 128\x22 xmlns=\x22h\
ttp://www.w3.org\
/2000/svg\x22>\x0a <g \
display=\x22none\x22>\x0a\
  <rect x=\x224\x22 y=\
\x2222\x22 width=\x2256\x22 \
height=\x2236\x22 rx=\x22\
2.8\x22 ry=\x222.8\x22 op\
acity=\x22.2\x22/>\x0a  <\
path d=\x22m4 46.2c\
0 1.551 1.2488 2\
.8 2.8 2.8h50.4c\
1.551 0 2.8-1.24\
9 2.8-2.8v-30.4c\
0-1.551-1.249-2.\
8-2.8-2.8h-25.2c\
-4.2 0-5.6-6-9.8\
-6h-15.4c-1.5512\
 0-2.8 1.2488-2.\
8 2.8\x22 fill=\x22#3a\
87e5\x22/>\x0a  <rect \
x=\x224\x22 y=\x2220\x22 wid\
th=\x2256\x22 height=\x22\
36\x22 rx=\x222.8\x22 ry=\
\x222.8\x22 opacity=\x22.\
2\x22/>\x0a  <rect x=\x22\
8\x22 y=\x2216\x22 width=\
\x2248\x22 height=\x2222\x22\
 rx=\x222.8\x22 ry=\x222.\
8\x22 fill=\x22#e4e4e4\
\x22/>\x0a  <rect x=\x224\
\x22 y=\x2221\x22 width=\x22\
56\x22 height=\x2236\x22 \
rx=\x222.8\x22 ry=\x222.8\
\x22 fill=\x22#93c0ea\x22\
/>\x0a  <path d=\x22M \
6.8008,7 C 5.249\
6,7 4,8.2496 4,9\
.8008 V 10.801 C\
 4,9.2496 5.2496\
,8 6.8008,8 H 22\
.199 c 4.2,0 5.6\
01,6 9.801,6 H 5\
7.199 C 58.75,14\
 60,15.25 60,16.\
801 v -1 C 60,14\
.25 58.75,13 57.\
199,13 H 32 C 27\
.8,13 26.399,7 2\
2.199,7 Z\x22 fill=\
\x22#ffffff\x22 opacit\
y=\x22.1\x22/>\x0a </g>\x0a \
<path d=\x22m20 12h\
88l4 4v96l-4 4h-\
88l-4-4v-96z\x22 fi\
ll=\x22#0000ff\x22 str\
oke=\x22#ffffff\x22 st\
roke-width=\x228\x22/>\
\x0a <path d=\x22m52 5\
6h12v36h-16 28\x22 \
fill=\x22#0000ff\x22 s\
troke=\x22#ffffff\x22 \
stroke-linecap=\x22\
square\x22 stroke-l\
inejoin=\x22bevel\x22 \
stroke-width=\x228\x22\
/>\x0a <ellipse cx=\
\x2264\x22 cy=\x2239\x22 rx=\
\x227.5\x22 ry=\x227\x22 fil\
l=\x22#ffffff\x22/>\x0a</\
svg>\x0a\
\x00\x00\x04;\
<\
svg version=\x221.1\
\x22 viewBox=\x220 0 1\
28 128\x22 xmlns=\x22h\
ttp://www.w3.org\
/2000/svg\x22>\x0a <g \
display=\x22none\x22>\x0a\
  <rect x=\x224\x22 y=\
\x2222\x22 width=\x2256\x22 \
height=\x2236\x22 rx=\x22\
2.8\x22 ry=\x222.8\x22 op\
acity=\x22.2\x22/>\x0a  <\
path d=\x22m4 46.2c\
0 1.551 1.2488 2\
.8 2.8 2.8h50.4c\
1.551 0 2.8-1.24\
9 2.8-2.8v-30.4c\
0-1.551-1.249-2.\
8-2.8-2.8h-25.2c\
-4.2 0-5.6-6-9.8\
-6h-15.4c-1.5512\
 0-2.8 1.2488-2.\
8 2.8\x22 fill=\x22#3a\
87e5\x22/>\x0a  <rect \
x=\x224\x22 y=\x2220\x22 wid\
th=\x2256\x22 height=\x22\
36\x22 rx=\x222.8\x22 ry=\
\x222.8\x22 opacity=\x22.\
2\x22/>\x0a  <rect x=\x22\
8\x22 y=\x2216\x22 width=\
\x2248\x22 height=\x2222\x22\
 rx=\x222.8\x22 ry=\x222.\
8\x22 fill=\x22#e4e4e4\
\x22/>\x0a  <rect x=\x224\
\x22 y=\x2221\x22 width=\x22\
56\x22 height=\x2236\x22 \
rx=\x222.8\x22 ry=\x222.8\
\x22 fill=\x22#93c0ea\x22\
/>\x0a  <path d=\x22M \
6.8008,7 C 5.249\
6,7 4,8.2496 4,9\
.8008 V 10.801 C\
 4,9.2496 5.2496\
,8 6.8008,8 H 22\
.199 c 4.2,0 5.6\
01,6 9.801,6 H 5\
7.199 C 58.75,14\
 60,15.25 60,16.\
801 v -1 C 60,14\
.25 58.75,13 57.\
199,13 H 32 C 27\
.8,13 26.399,7 2\
2.199,7 Z\x22 fill=\
\x22#ffffff\x22 opacit\
y=\x22.1\x22/>\x0a </g>\x0a \
<circle cx=\x2264\x22 \
cy=\x2262.999996\x22 r\
=\x2250.999996\x22 fil\
l=\x22#ff0000\x22 stro\
ke=\x22#ffffff\x22 str\
oke-linecap=\x22squ\
are\x22 stroke-line\
join=\x22bevel\x22 str\
oke-width=\x228.000\
01\x22/>\x0a <rect x=\x22\
36\x22 y=\x2254\x22 width\
=\x2256\x22 height=\x2220\
\x22 fill=\x22#ffffff\x22\
/>\x0a</svg>\x0a\
\x00\x00\x04f\
<\
svg version=\x221.1\
\x22 viewBox=\x220 0 1\
28 128\x22 xmlns=\x22h\
ttp://www.w3.org\
/2000/svg\x22>\x0a <g \
display=\x22none\x22>\x0a\
  <rect x=\x224\x22 y=\
\x2222\x22 width=\x2256\x22 \
height=\x2236\x22 rx=\x22\
2.8\x22 ry=\x222.8\x22 op\
acity=\x22.2\x22/>\x0a  <\
path d=\x22m4 46.2c\
0 1.551 1.2488 2\
.8 2.8 2.8h50.4c\
1.551 0 2.8-1.24\
9 2.8-2.8v-30.4c\
0-1.551-1.249-2.\
8-2.8-2.8h-25.2c\
-4.2 0-5.6-6-9.8\
-6h-15.4c-1.5512\
 0-2.8 1.2488-2.\
8 2.8\x22 fill=\x22#3a\
87e5\x22/>\x0a  <rect \
x=\x224\x22 y=\x2220\x22 wid\
th=\x2256\x22 height=\x22\
36\x22 rx=\x222.8\x22 ry=\
\x222.8\x22 opacity=\x22.\
2\x22/>\x0a  <rect x=\x22\
8\x22 y=\x2216\x22 width=\
\x2248\x22 height=\x2222\x22\
 rx=\x222.8\x22 ry=\x222.\
8\x22 fill=\x22#e4e4e4\
\x22/>\x0a  <rect x=\x224\
\x22 y=\x2221\x22 width=\x22\
56\x22 height=\x2236\x22 \
rx=\x222.8\x22 ry=\x222.8\
\x22 fill=\x22#93c0ea\x22\
/>\x0a  <path d=\x22M \
6.8008,7 C 5.249\
6,7 4,8.2496 4,9\
.8008 V 10.801 C\
 4,9.2496 5.2496\
,8 6.8008,8 H 22\
.199 c 4.2,0 5.6\
01,6 9.801,6 H 5\
7.199 C 58.75,14\
 60,15.25 60,16.\
801 v -1 C 60,14\
.25 58.75,13 57.\
199,13 H 32 C 27\
.8,13 26.399,7 2\
2.199,7 Z\x22 fill=\
\x22#ffffff\x22 opacit\
y=\x22.1\x22/>\x0a </g>\x0a \
<path d=\x22m59 8h1\
0l51 104-4 4h-10\
4l-4-4z\x22 fill=\x22#\
ffcc00\x22 stroke=\x22\
#ffffff\x22 stroke-\
width=\x228\x22/>\x0a <ci\
rcle cx=\x2264\x22 cy=\
\x2296\x22 r=\x229\x22 fill=\
\x22#000000\x22/>\x0a <pa\
th d=\x22m56 43h16l\
-3 41h-10z\x22 fill\
=\x22#000000\x22 strok\
e-linecap=\x22squar\
e\x22 stroke-linejo\
in=\x22bevel\x22 strok\
e-width=\x228\x22/>\x0a</\
svg>\x0a\
"

qt_resource_name = b"\
\x00\x06\
\x06\x8a\x9c\xb3\
\x00a\
\x00s\x00s\x00e\x00t\x00s\
\x00\x03\
\x00\x00n\xb9\
\x00g\
\x00u\x00i\
\x00\x08\
\x08\x01Z\x5c\
\x00m\
\x00a\x00i\x00n\x00.\x00q\x00m\x00l\
\x00\x0e\
\x01D\xa4\xc7\
\x00a\
\x00l\x00e\x00r\x00t\x00-\x00i\x00n\x00f\x00o\x00.\x00s\x00v\x00g\
\x00\x10\
\x0f8\x11g\
\x00a\
\x00l\x00e\x00r\x00t\x00-\x00d\x00a\x00n\x00g\x00e\x00r\x00.\x00s\x00v\x00g\
\x00\x11\
\x03u\xfa\x07\
\x00a\
\x00l\x00e\x00r\x00t\x00-\x00w\x00a\x00r\x00n\x00i\x00n\x00g\x00.\x00s\x00v\x00g\
\
"

qt_resource_struct = b"\
\x00\x00\x00\x00\x00\x02\x00\x00\x00\x02\x00\x00\x00\x01\
\x00\x00\x00\x00\x00\x00\x00\x00\
\x00\x00\x00\x12\x00\x02\x00\x00\x00\x01\x00\x00\x00\x07\
\x00\x00\x00\x00\x00\x00\x00\x00\
\x00\x00\x00\x00\x00\x02\x00\x00\x00\x01\x00\x00\x00\x03\
\x00\x00\x00\x00\x00\x00\x00\x00\
\x00\x00\x00\x00\x00\x02\x00\x00\x00\x03\x00\x00\x00\x04\
\x00\x00\x00\x00\x00\x00\x00\x00\
\x00\x00\x004\x00\x00\x00\x00\x00\x01\x00\x00\x06\xe5\
\x00\x00\x01\x88\xc5C )\
\x00\x00\x00|\x00\x00\x00\x00\x00\x01\x00\x00\x0f\xae\
\x00\x00\x01\x88\xcf\x1f@\xe4\
\x00\x00\x00V\x00\x00\x00\x00\x00\x01\x00\x00\x0bo\
\x00\x00\x01\x88\xcf\x1d\xf2\xad\
\x00\x00\x00\x12\x00\x02\x00\x00\x00\x01\x00\x00\x00\x08\
\x00\x00\x00\x00\x00\x00\x00\x00\
\x00\x00\x00\x1e\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\
\x00\x00\x01\x88\xd0\xbb\x01\x08\
"

def qInitResources():
    QtCore.qRegisterResourceData(0x03, qt_resource_struct, qt_resource_name, qt_resource_data)

def qCleanupResources():
    QtCore.qUnregisterResourceData(0x03, qt_resource_struct, qt_resource_name, qt_resource_data)

qInitResources()
