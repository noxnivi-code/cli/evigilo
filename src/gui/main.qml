// main.qml
//
// evigilo v. 0.1.0
// (C) 202306 NoxNivi
//
// GUI file for the simple Alert app

// imports
import QtQuick
import QtQuick.Window
import QtQuick.Controls
import QtQuick.Layouts

// local imports

Window {
  id: root

  height: evigiloContent.implicitHeight + 40
  width: evigiloContent.implicitWidth + 40
  minimumWidth: 250
  maximumWidth: 450

  property string bcolor: ""
  property string btype: ""
  property string bmessage: ""
  property string bicon: ""

  Component.onCompleted: visible = true

  Pane {
    id: windowBG
    anchors.fill: parent
    padding: 10
    // anchors.left: parent.left
    // anchors.right: parent.right
    // implicitWidth: evigiloContent.implicitWidth
    // implicitHeight: evigiloContent.implicitHeight

    ColumnLayout {
      id: evigiloContent
      anchors.left: parent.left
      anchors.right: parent.right
      anchors.top: parent.top
      anchors.bottom: parent.bottom

      RowLayout {
        id: messageContent
        Layout.fillHeight: true
        Layout.fillWidth: true
        spacing: 20

        Image {
          id: messageIcon
          source: bicon
          Layout.alignment: Qt.AlignTop
        }

        ColumnLayout {
          id: rightSide
          Layout.fillHeight: true
          Layout.fillWidth: true
          Layout.margins: 0
          Layout.bottomMargin: 20
          spacing: 10

          Label {
            id: alertLabel
            Layout.fillWidth: true
            text: btype
            font.pixelSize: alertMessage.font.pixelSize * 2
            bottomPadding: 10
            font.bold: true
          }

          Text {
            id: alertMessage
            Layout.fillHeight: true
            Layout.fillWidth: true
            text: bmessage
            color: alertLabel.color
            wrapMode: Text.WordWrap
          }
        }
      }

      Button {
        id: quitButton
        text: "OK"
        Layout.alignment: Qt.AlignRight

        onClicked: {
          Qt.quit()
        }
      }
    }
  }
}
