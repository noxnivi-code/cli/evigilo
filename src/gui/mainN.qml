// main.qml
//
// evigilo v. 0.1.0
// (C) 202306 NoxNivi
//
// GUI file for the simple Alert app

// imports
import QtQuick
import QtQuick.Window
import QtQuick.Controls
import QtQuick.Layouts

// local imports

Window {
  id: root
  minimumWidth: 480
  //minimumHeight: 200
  implicitHeight: windowLayout.implicitHeight
  //minimumHeight: alertMessage.height + 80

  property string bcolor: ""
  property string btype: ""
  property string bmessage: ""
  property string bicon: ""

  Component.onCompleted: visible = true

  ColumnLayout{
    id: windowLayout
    anchors.fill: parent
    spacing: 0

    Pane {
      id: rootContent
      Layout.fillWidth: true
      Layout.fillHeight: true
      padding: 20

      ColumnLayout {
        id: colContent
        anchors.fill: parent
        spacing: 10
        Layout.bottomMargin: 20

        RowLayout {
          id: alertContent
          Layout.fillHeight: true
          Layout.fillWidth: true
          spacing: 20

          Image {
            id: imIcon
            source: bicon
            Layout.alignment: Qt.AlignTop
          }

          ColumnLayout {
            id: rightSide
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.margins: 0
            Layout.bottomMargin: 20
            spacing: 10

            Label {
              id: alertLabel
              Layout.fillWidth: true
              text: btype
              font.pixelSize: alertMessage.font.pixelSize * 2
              bottomPadding: 10
              font.bold: true
            }

            Text {
              id: alertMessage
              Layout.fillHeight: true
              Layout.fillWidth: true
              color: alertLabel.color
              text: bmessage
              wrapMode: Text.WordWrap
            }

          }
        }
        Button {
          id: quitButton
          text: "OK"
          Layout.alignment: Qt.AlignRight

          onClicked: {
            // console.log("exit clicked")
            Qt.quit()
          }

        }
      }
    }



    Rectangle {
      id: colorCode
      height: 12
      Layout.fillWidth: true
      color: bcolor
    
    }
  }
}
