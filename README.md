# Evigilo

### Project Status
As all the elements of the **NoxNivi** 'SET', this is a Work In Progress,
therefore development pace is slow.

## Description
Simple Alert Utility that is called from the command line or can be called from
another software. At the moment it can handle tree types of alerts: Info,
Warning and Danger.

## Screenshots
![Evigilo running on NoxNivi](/screenshots/screen.png "Evigilo")*Evigilo*


## Installation
**General Instructions**
This utility is released as a _zipapp_. Download the evigilo zipapp file to a folder of your computer that is in the path, or to any folder and then add it to the path.

**Arch Linux**
If user is running Arch Linux, or any Arch derived distro, add the NoxNivi repository and install Evigilo using pacman (pacman -S evigilo)

## Usage
From the terminal call the `evigilo` command 

```
Basic <evigilo> usage information
evigilo v.0.1.0
usage: evigilo <option> "message" (message should be inside quotes)
options:
     info: creates an Info Alert with the message "message"
     warning: creates a Warning Alert with the message "message"
     danger: creates a Danger Alert with the message "message"
     help: shows this help
```

## Support

## License
THis software is released under the GPLv3 license. A copy of said license is provided in this folder.
